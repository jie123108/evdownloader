#ifndef __DOWN_LOGGER_H__
#define __DOWN_LOGGER_H__

extern int64_t GetFileSize(int file);

void ReleasePrint(const char* LEVEL, const char* funcName, 
			const char* fileName, int line,  const char* format,  ...);

#define LOG_DEBUG(format, args...) \
		ReleasePrint("DEBUG", __FUNCTION__, __FILE__, __LINE__, format, ##args)
#define LOG_INFO(format, args...) \
		ReleasePrint(" INFO", __FUNCTION__, __FILE__, __LINE__, format, ##args)
#define LOG_WARN(format, args...) \
		ReleasePrint(" WARN", __FUNCTION__, __FILE__, __LINE__, format, ##args)
#define LOG_ERROR(format, args...) \
		ReleasePrint("ERROR", __FUNCTION__, __FILE__, __LINE__, format, ##args)

#endif
