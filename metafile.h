#ifndef __METAFILE_H__
#define __METAFILE_H__
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
	char magic[4];//"SDL\0"
	int64_t filesize; //文件大小.
	int32_t block_size; //分块大小
	int32_t block_cnt; //块总数
	int32_t block_downloading; //正在下载中的数量 (每次从metafile中加载时，要重新设置为0)
	int32_t block_download; //已经下载完成的。(每次从metafile中加载时，要重新修改该值)
	char bits[0];
}meta_info_t;

#define META_HEAD_SIZE sizeof(meta_info_t)

//根据文件大小，块大小计算块数。
#define blocks(filesize, block_size) ((filesize+block_size-1)/block_size)

//可进行下载的块的数量。(指未下载完，也不在下载中的块)
#define downloadable_blocks(mi) (mi->block_cnt-mi->block_download-mi->block_downloading)
#define not_download_blocks(mi) (mi->block_cnt - mi->block_download)

#define CHAR_SIZE 8
#define BITMASK(b) (1 << ((b) % CHAR_SIZE))
#define BITSLOT(b) ((b) / CHAR_SIZE)
#define BITSET(arr, b) ((arr)[BITSLOT(b)] |= BITMASK(b))
#define BITCLEAR(arr, b) ((arr)[BITSLOT(b)] &= ~BITMASK(b))
#define BITTEST(arr, b) ((arr)[BITSLOT(b)] & BITMASK(b))
#define BITNSLOTS(nb) ((nb + CHAR_SIZE - 1)/CHAR_SIZE)
#define BITARRAY(arr, size); char arr[BITNSLOTS(size)];

typedef enum {
	FLAG_DL_BEGIN=0,  //开始下载状态位
	FLAG_DL_OK=1 		//下载完成状态位
}BIT_FLAG_T;


#define CHECK_BLOCK_INDEX(meta_info, block_index) { \
	int __block_cnt = blocks(meta_info->filesize, meta_info->block_size);\
	if(block_index >= __block_cnt){\
		LOG_ERROR("block_index(%d) > block_cnt(%d)", block_index, __block_cnt);\
		return -1;\
	}\
}

/**
 * 设置某一个块开始下载。
 */
int meta_info_set_block_downloading(meta_info_t* meta_info, int block_index);
int meta_info_is_block_downloading(meta_info_t* meta_info, int block_index);
int meta_info_clear_block_downloading(meta_info_t* meta_info, int block_index);
int meta_info_set_block_download_ok(meta_info_t* meta_info, int block_index);
int meta_info_is_block_download_ok(meta_info_t* meta_info, int block_index);

void meta_info_show(meta_info_t* meta_info);

#endif

