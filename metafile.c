#include "metafile.h"
#include "Logger.h"
#include <assert.h>

inline void block_bit_set(char* bits, int block_index, BIT_FLAG_T flag)
{
	int i = block_index*2+(int)flag;
	BITSET(bits, i);
}
inline void block_bit_clear(char* bits, int block_index, BIT_FLAG_T flag)
{
	int i = block_index*2+(int)flag;
	BITCLEAR(bits, i);
}
inline int block_bit_test(char* bits, int block_index, BIT_FLAG_T flag)
{
	int i = block_index*2+(int)flag;
	return BITTEST(bits, i)>0?1:0;
}

/**
 * 设置某一个块开始下载。
 */
int meta_info_set_block_downloading(meta_info_t* meta_info, int block_index)
{
	assert(meta_info != NULL);
	assert(meta_info->filesize > 0);
	assert(meta_info->block_size > 0);
	assert(meta_info->bits != NULL);
	
	CHECK_BLOCK_INDEX(meta_info, block_index);
	meta_info->block_downloading++;
	
	LOG_DEBUG(" block [%d] begin downloading..", block_index);
	block_bit_set(meta_info->bits, block_index, FLAG_DL_BEGIN);
	return 0;
}

int meta_info_is_block_downloading(meta_info_t* meta_info, int block_index)
{
	assert(meta_info != NULL);
	assert(meta_info->filesize > 0);
	assert(meta_info->block_size > 0);
	assert(meta_info->bits != NULL);
	
	CHECK_BLOCK_INDEX(meta_info, block_index);

	return block_bit_test(meta_info->bits, block_index, FLAG_DL_BEGIN);
}

int meta_info_clear_block_downloading(meta_info_t* meta_info, int block_index)
{
	assert(meta_info != NULL);
	assert(meta_info->filesize > 0);
	assert(meta_info->block_size > 0);
	assert(meta_info->bits != NULL);
	
	CHECK_BLOCK_INDEX(meta_info, block_index);
	LOG_DEBUG("clear block [%d] downloading..", block_index);
	if(meta_info->block_downloading > 0){
		meta_info->block_downloading--;
	}
	block_bit_clear(meta_info->bits, block_index, FLAG_DL_BEGIN);
	return 0;
}

int meta_info_set_block_download_ok(meta_info_t* meta_info, int block_index)
{
	assert(meta_info != NULL);
	assert(meta_info->filesize > 0);
	assert(meta_info->block_size > 0);
	assert(meta_info->bits != NULL);
	
	CHECK_BLOCK_INDEX(meta_info, block_index);
	LOG_DEBUG("set block [%d] download ok..", block_index);
	// TODO: 锁。
	meta_info->block_download++;
	if(meta_info->block_downloading > 0){
		meta_info->block_downloading--;
	}
	
	block_bit_set(meta_info->bits, block_index, FLAG_DL_OK);
	return 0;
}


int meta_info_is_block_download_ok(meta_info_t* meta_info, int block_index)
{
	assert(meta_info != NULL);
	assert(meta_info->filesize > 0);
	assert(meta_info->block_size > 0);
	assert(meta_info->bits != NULL);
	
	CHECK_BLOCK_INDEX(meta_info, block_index);

	return block_bit_test(meta_info->bits, block_index, FLAG_DL_OK);
}

void meta_info_show(meta_info_t* meta_info)
{
	LOG_INFO("magic:%s, filesize:%d, block_size:%d, \n"
			 "block_cnt:%d,downloading:%d,download_ok:%d\n", 
				meta_info->magic, (int)meta_info->filesize, meta_info->block_size,
				meta_info->block_cnt,meta_info->block_downloading,meta_info->block_download
				);

	int i,x,downloading, download_ok, idx;
	for(i=0;i<meta_info->block_cnt;i+=16){
		// 显示 i .. i+16 个块的状态。
		char buf_downloading[17];
		memset(buf_downloading,0, sizeof(buf_downloading));
		char buf_download_ok[17];
		memset(buf_download_ok,0, sizeof(buf_download_ok));
		
		for(x = 0;x<16;x++){
			idx = x+i;
			if(idx < meta_info->block_cnt){
				downloading = meta_info_is_block_downloading(meta_info, idx);
				download_ok = meta_info_is_block_download_ok(meta_info, idx);
				buf_downloading[x] = downloading?'+':'-';
				buf_download_ok[x] = download_ok?'+':'-';
			}else{
				buf_downloading[x] = ' ';
				buf_download_ok[x] = ' ';
			}
		}

		LOG_INFO("%3d ~ %3dM downloading: %s", i, i+1, buf_downloading);
		LOG_INFO("%3d ~ %3dM download_ok: %s", i, i+1, buf_download_ok);
	}
}

#ifdef METAFILE_MAIN
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>

int main(int argc, char* argv[])
{
	if(argc < 2){
		printf("Usage: %s <metafile>\n", argv[0]);
		exit(1);
	}

	const char* metafilename = argv[1];
	int file = open(metafilename, O_RDONLY);
	if(file < 1){
		printf("open metafile [%s] failed! err:%s\n", metafilename, strerror(errno));
		exit(2);
	}

	int64_t metafilesize = GetFileSize(file);
	if(metafilesize <= 0){
		printf("get file [%s] size failed!\n", metafilename);
		exit(3);
	}

	meta_info_t* meta_info  = (meta_info_t*)mmap(NULL, metafilesize, PROT_READ, MAP_SHARED, file, 0);
	if(meta_info == MAP_FAILED){
		if(errno == ENOMEM){
			LOG_ERROR("mmap failed no memory!");
		}else{
			LOG_ERROR("mmap failed! %d err:%s", errno, strerror(errno));
		}
		close(file);
		exit(4);
	}

	meta_info_show(meta_info);
	munmap(meta_info, metafilesize);

	exit(0);
}
#endif
