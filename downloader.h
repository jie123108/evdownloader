#ifndef __SUPER_DOWNLOADER_H__
#define  __SUPER_DOWNLOADER_H__
#include "event2/http.h"

#define HTTP_SPECIAL_RESPONSE          300
#define HTTP_MOVED_PERMANENTLY         HTTP_MOVEPERM
#define HTTP_MOVED_TEMPORARILY         HTTP_MOVETEMP
#define HTTP_NOT_MODIFIED              HTTP_NOTMODIFIED
#define HTTP_TEMPORARY_REDIRECT        307
#define HTTP_BAD_REQUEST               HTTP_BADREQUEST
#define HTTP_UNAUTHORIZED              401
#define HTTP_FORBIDDEN                 403
#define HTTP_NOT_FOUND                 HTTP_NOTFOUND
#define HTTP_NOT_ALLOWED               HTTP_BADMETHOD
#define HTTP_REQUEST_TIME_OUT          408
#define HTTP_CONFLICT                  409
#define HTTP_INTERNAL_SERVER_ERROR     HTTP_INTERNAL
#define HTTP_NOT_IMPLEMENTED           HTTP_NOTIMPLEMENTED
#define HTTP_BAD_GATEWAY               502
#define HTTP_SERVICE_UNAVAILABLE       HTTP_SERVUNAVAIL
#define HTTP_GATEWAY_TIME_OUT          504

int http_get(const char* url, char** resp);


#endif