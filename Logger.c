#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>

inline int64_t GetFileSize(int file)
{
	struct stat info;
	if(fstat(file, &info) != -1)
	{
		return (uint64_t)info.st_size;
	}

	return -1;
}

#define LOG_TF "%02d-%02d %02d:%02d:%02d "
#define LOG_SHOW_TIME 1
#define LOG_BUF_LEN 2048


void ReleasePrint(const char* LEVEL, const char* funcName, 
			const char* fileName, int line,  const char* format,  ...){
	va_list   args;
	va_start(args,   format); 
	char vsp_buf[LOG_BUF_LEN];	
	memset(vsp_buf, 0, LOG_BUF_LEN);
	int pos = 0; 
#if LOG_SHOW_TIME
	//不显示日志时间。
	time_t timep;
	struct tm *ptm, mytm;
	time(&timep);
	ptm = localtime_r(&timep, &mytm); 
	pos = snprintf(vsp_buf + pos, LOG_BUF_LEN-pos, LOG_TF "%s:%s[%s:%d] ", 
			1+ptm->tm_mon, ptm->tm_mday,  
			ptm->tm_hour, ptm->tm_min, ptm->tm_sec, 
			LEVEL, funcName, fileName, line); 
#else
	pos = snprintf(vsp_buf + pos, LOG_BUF_LEN-pos, "%s:%s[%s:%d] ",
			LEVEL, funcName, fileName, line); 
#endif
	pos += vsnprintf(vsp_buf +pos ,  LOG_BUF_LEN-pos, format , args);
	if(pos < LOG_BUF_LEN-1){ 
 		pos += snprintf(vsp_buf + pos, LOG_BUF_LEN-pos, "\n");
	}else{
		vsp_buf[LOG_BUF_LEN-2] = '\n';
		vsp_buf[LOG_BUF_LEN-1] = 0;
		pos = LOG_BUF_LEN;
	}
	fprintf(stderr, "%.*s", pos, vsp_buf);
	
	va_end(args); 
}


