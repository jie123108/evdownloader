#include "event2/event.h"
#include "event2/http.h"
#include "downloader.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>
#include "metafile.h"
#include "Logger.h"

#define my_calloc(nmemb, size) calloc(nmemb, size)
#define my_strdup(p) strdup(p)
#define my_free(ptr); if(ptr){ free(ptr);ptr=NULL;}


typedef struct event_base event_base;
typedef struct evhttp_request evhttp_request;
typedef struct evhttp_connection evhttp_connection;
typedef struct evkeyvalq evkeyvalq;
typedef struct evbuffer evbuffer;
typedef struct evhttp_uri evhttp_uri;

typedef struct download_block_s download_block_t;

struct block_list_s{
	download_block_t* block;
	struct block_list_s* next;
};

typedef struct block_list_s block_list_t;

typedef enum {
	dl_ok = 0, //正常下载中，未出现错误。
	dl_err_connect, //无法连接指定的服务及端口
	dl_err_http, //HTTP 错误
	dl_err_content_range, // HTTP 服务器返回了非法的Content-Range
	dl_err_write_metafile, //写入，修改metafile出错。
	dl_err_mmap_metafile, //metafile做mmap映射时出错。
	dl_err_write_file, //写入本地文件时出错。
	
	dl_err_cnt
}download_error_t;

typedef struct {
	char host[32];
	int port;
	int timeout; //second
}http_config_t;

typedef struct {
	event_base* base;

	http_config_t http_cfg;
	char* uri; //要下载的url.
	evhttp_uri* http_uri;
	
	char* filename; //本地文件名。
	int file;

	char* metafilename; //meta filename
	int32_t metafile_size; //meta 文件大小
	int metafile;
	
	int block_size;
	meta_info_t* meta_info;
	block_list_t* dl_blocks; //正在下载中的块。

	int threads_max; //设置的最大线程数
	int threads_cur; //当前正在下载的线程数。

	download_error_t dl_err_type; //下载错误类型
	int dl_errno; 				//错误码，当错误类型是dl_err_http时，dl_errno保存http错误码。

}download_file_t;

struct download_block_s{
	download_file_t* dlf;
	int block_index;
	int writed; //已经存储到文件中的字节数。(当前块)
	evhttp_connection* conn;

	download_error_t dl_err_type; //下载错误类型
	int dl_errno; 				//错误码，当错误类型是dl_err_http时，dl_errno保存http错误码。
};

evhttp_connection* dlf_conn_get(download_file_t* dlf)
{
	// TODO: 连接池。
	http_config_t* config = &dlf->http_cfg;
	evhttp_connection* conn = evhttp_connection_base_new(
					dlf->base, NULL, config->host, config->port);
	LOG_INFO("host:%s,port:%d, timeout:%d", config->host, config->port, config->timeout);
	if(config->timeout > 0){
		evhttp_connection_set_timeout(conn, config->timeout);
	}
	
	return conn;	
}

void dlf_conn_put(download_file_t* dlf, evhttp_connection* conn)
{
	evhttp_connection_free(conn);
}

int download_file_destroy(download_file_t* dlf)
{
	LOG_DEBUG("download file destroy!");
	if(dlf == NULL){
		return -1;
	}
	my_free(dlf->uri);
	my_free(dlf->filename);
	my_free(dlf->metafilename);

	if(dlf->meta_info != NULL){
		assert(dlf->metafile_size > 0);
		int ret = munmap(dlf->meta_info, dlf->metafile_size);
		if(ret != 0){
			LOG_ERROR("munmap failed! %d err:%s",errno, strerror(errno));
		}
		dlf->meta_info = NULL;
	}

	if(dlf->http_uri != NULL){
		evhttp_uri_free(dlf->http_uri);
		dlf->http_uri = NULL;
	}
	
	if(dlf->file > 0){
		close(dlf->file);
		dlf->file = 0;
	}
	if(dlf->metafile > 0){
		close(dlf->metafile);
		dlf->metafile = 0;
	}
	my_free(dlf);
	
	return 0;
}

int dlf_block_is_downloading(download_file_t* dlf, int block_index)
{
	int is_downloading = 0;
	if(dlf != NULL && dlf->dl_blocks != NULL){
		block_list_t* bll = dlf->dl_blocks;
		while(bll){
			if(bll->block->block_index == block_index){
				is_downloading = 1;
				break;
			}
			bll = bll->next;
		}
	}
	return is_downloading;
}

/**
 * 从剩余的块中选择，一到(dlf->threads_max-dlf->threads_cur)个进行下载
 * 返回值: 返回重新启动了几个块(0:表示没有启动任何新块的下载)
 * 
 */
int dlf_start_new_blocks_dl(download_file_t* dlf)
{
	assert(dlf->meta_info != NULL);
	meta_info_t* meta_info = dlf->meta_info;
	int n = dlf->threads_max-dlf->threads_cur;
	int started_block = 0;
	int i;
	for(i=0;i<n && downloadable_blocks(meta_info)> 0;i++){
		//选择一个需要下载的块进行下载。
		int x;
		// TODO: 使用指针记录位置.
		for(x=0;x<meta_info->block_cnt;x++){
			//已经下载完的。
			if(meta_info_is_block_download_ok(meta_info, x)){
				continue;
			}
			if(meta_info_is_block_downloading(meta_info, x)){
				continue;
			}
			//可以下载的块
			LOG_DEBUG("find Block [%d]", x);
			int ret = http_download_block(dlf, x);
			if(ret != 0){
				LOG_ERROR("start http_download_block(%d) failed!", x);
			}else{
				started_block++;
				break;
			}
		}
	}

	return started_block;
}

/**
 * 统计正在下载中的块数
 */
inline int dlf_dlblock_cnt(download_file_t* dlf)
{
	int downloading_cnt = 0;
	block_list_t* bl = dlf->dl_blocks;
	while(bl){
		downloading_cnt++;
		bl = bl->next;
	}
	return downloading_cnt;
}


int dlf_dlblock_add(download_file_t* dlf, download_block_t* block)
{
	// TODO: 加锁，线程安全
	LOG_DEBUG("add block: %d", block->block_index);
	block_list_t* node = (block_list_t*)my_calloc(1, sizeof(block_list_t));
	node->block = block;
	
	block_list_t** pblocks = &dlf->dl_blocks;
	while(*pblocks != NULL){
		pblocks = &(*pblocks)->next;
	}
	*pblocks = node;
	// TODO: 使用原子变量
	dlf->threads_cur++;

	int dl_blocks = dlf_dlblock_cnt(dlf);
	LOG_DEBUG("thread_cur:%d, downloading blocks:%d", dlf->threads_cur, dl_blocks);
	assert(dlf->threads_cur == dl_blocks);
	
	return 0;
}

int dlf_dlblock_del(download_file_t* dlf, download_block_t* block)
{
	LOG_DEBUG("del block: %d", block->block_index);
	// TODO: 加锁，线程安全
	block_list_t* pre_node = NULL;
	block_list_t* block_list = dlf->dl_blocks;
	int find_node = 0;
	while(block_list){
		if(block_list->block == block){//找到了，删除掉吧。
			if(pre_node == NULL){//首节点。
				dlf->dl_blocks = block_list->next;
			}else{
				pre_node->next = block_list->next;
			}
			my_free(block_list);
			find_node = 1;
			break;
		}

		pre_node = block_list;
		block_list = block_list->next;
	}
	assert(find_node > 0);
	
	my_free(block);
	dlf->threads_cur--;
	assert(dlf->threads_cur >=0);
	
	int dl_blocks = dlf_dlblock_cnt(dlf);
	LOG_DEBUG("thread_cur:%d, downloading blocks:%d", dlf->threads_cur, dl_blocks);
	assert(dlf->threads_cur == dl_blocks);
}


void finish_write(struct evhttp_request * req, void* args)
{
	download_block_t* dlb = (download_block_t*)args;
	assert(dlb != NULL);
	assert(dlb->dlf != NULL);
	download_file_t* dlf = dlb->dlf;
	
	int rsp_code = 0;
	if(req == NULL){
		rsp_code = HTTP_INTERNAL_SERVER_ERROR;
		dlb->dl_err_type = dl_err_http;
		dlb->dl_errno = rsp_code;
	}else{
		rsp_code = evhttp_request_get_response_code(req);
	}
	LOG_DEBUG("block[%d] finish_write, http_code:%d",dlb->block_index, rsp_code);
	
	//evkeyvalq* input_headers = evhttp_request_get_input_headers(req);
	int download_ok = 0;
	if(dlb->dl_err_type == dl_ok && (rsp_code == HTTP_OK || rsp_code == 206)){
		download_ok = 1;
		struct evbuffer* buf = evhttp_request_get_input_buffer(req);  
	    size_t len = evbuffer_get_length(buf);
		assert(len <= 0);

		int block_size = dlf->block_size;
		assert(dlf->meta_info != NULL);
		int block_cnt = blocks(dlf->meta_info->filesize, dlf->block_size);
		if(block_cnt-1 == dlb->block_index){//当前block是最后一个块。
			block_size = dlf->meta_info->filesize % dlf->block_size;
			if(block_size == 0){
				block_size = dlf->block_size;
			}
		}
		
		assert(dlb->writed <= block_size);
		if(dlb->writed == block_size){//块已经下载完成了。
			meta_info_set_block_download_ok(dlf->meta_info, dlb->block_index);
		}else{
			LOG_ERROR("file [%s->%s] block [%d] download failed! dlb->writed(%d) < block_size(%d)",
						dlf->uri,dlf->filename, dlb->block_index,
						dlb->writed, dlf->block_size);
			// TODO: 错误处理。重新下载本块。
			meta_info_clear_block_downloading(dlf->meta_info, dlb->block_index);
		}

	}else{
		LOG_ERROR("file [%s->%s] block [%d] download failed! dl_error:%d, http_code:%d",
						dlf->uri,dlf->filename, dlb->block_index,
						(int)dlb->dl_err_type, rsp_code);
		if(dlf->meta_info != NULL){
			meta_info_clear_block_downloading(dlf->meta_info, dlb->block_index);
		}
	}

	download_error_t err_type = dlb->dl_err_type;
	int dl_errno = dlb->dl_errno;
	
	dlf_conn_put(dlf, dlb->conn);
	dlf_dlblock_del(dlf, dlb);
	dlb = NULL;

	if(download_ok){
		int new_started = dlf_start_new_blocks_dl(dlf);
		if(new_started==0 && not_download_blocks(dlf->meta_info) == 0){
			LOG_INFO("####### file [%s => %s] is download ok ########",dlf->uri, dlf->filename);

			// TODO: 调用处理成功的回调。
			event_base_loopexit(dlf->base, 0);
			download_file_destroy(dlf);
			dlf = NULL;
		}
	}else{
		switch(err_type){
		case dl_err_http: //HTTP 错误
		// TODO: 重新下载的处理。
		/**
		switch(dl_errno){
		case HTTP_SPECIAL_RESPONSE:
		case HTTP_MOVED_PERMANENTLY:
		case HTTP_MOVED_TEMPORARILY:
		case HTTP_NOT_MODIFIED:
		case HTTP_TEMPORARY_REDIRECT:
		case HTTP_BAD_REQUEST:
		case HTTP_UNAUTHORIZED:
		case HTTP_FORBIDDEN:
		case HTTP_NOT_FOUND:
		case HTTP_NOT_ALLOWED:
		case HTTP_NOT_IMPLEMENTED:
		case HTTP_BAD_GATEWAY:
		case HTTP_INTERNAL_SERVER_ERROR:
		case HTTP_GATEWAY_TIME_OUT:

		break;
		}**/
		
		LOG_DEBUG("http_error: %d", dl_errno);
		goto DOWNLOAD_FAIL;
		break;
		case dl_err_content_range: // HTTP 服务器返回了非法的Content-Range
		case dl_err_write_metafile: //写入，修改metafile出错。
		case dl_err_mmap_metafile: //metafile做mmap映射时出错。
		case dl_err_write_file: //写入本地文件时出错。
		default:
		DOWNLOAD_FAIL:
		{
			// TODO: 调用处理失败的回调
			dlf->dl_err_type = err_type;
			dlf->dl_errno = dl_errno;
			if(dlf->threads_cur == 0){
				//已经是最后的下载连接了，
				event_base_loopexit(dlf->base, 0);
				download_file_destroy(dlf);
				dlf = NULL;
			}else{ //还有其它连接在处理。
				//什么也不做，等待其它连接处理完成后，再执行if处的逻辑。
			}
		}
		}
	}
	
	//event_base_loopexit(dlb->base, 0);  
	//LOG_DEBUG("################### free dlb ############");
	//free(dlb);
}

void chunked_write(struct evhttp_request* req, void * args)
{
	
	download_block_t* dlb = (download_block_t*)args;
	download_file_t* dlf = dlb->dlf;
	assert(dlf != NULL);
	
	int ret = 0;
	int rsp_code = evhttp_request_get_response_code(req);
	LOG_INFO("block[%d] file [%s => %s] http_code:%d",dlb->block_index,
			dlf->uri, dlf->filename, rsp_code);
	
	if(rsp_code == HTTP_OK || rsp_code == 206){
		if(dlb->block_index == 0 && dlb->writed == 0){
			evkeyvalq* headers_in = evhttp_request_get_input_headers(req);
			const char* rsp_content_range = evhttp_find_header(headers_in, "Content-Range");
			LOG_INFO("block[%d] rsp_range:%s", dlb->block_index, rsp_content_range);
			int begin=0;
			int end = 0;
			int content_length = 0;
			int x = sscanf(rsp_content_range, "bytes %d-%d/%d", &begin, &end, &content_length);
			if(x != 3){
				LOG_ERROR("block[%d] Invalid [Content-Range: %s]", dlb->block_index, rsp_content_range);
				//3 可能有问题。
				evhttp_cancel_request(req);
				dlb->dl_err_type = dl_err_content_range;
				return;
			}
			//每个块占用2bit(1bit表示，开始下载，2bit表示完成下载)
			
			int bits = blocks(content_length,dlf->block_size)*2;
			int bytes = (bits+7)/8; 
			int meta_file_size = META_HEAD_SIZE+bytes;
			LOG_DEBUG("block[%d] meta_file_size:%d", dlb->block_index,meta_file_size);
			ret = ftruncate(dlf->metafile, meta_file_size);
			if(ret != 0){
				dlb->dl_err_type = dl_err_write_metafile;
				dlb->dl_errno = errno;
				//3 可能有问题。
				evhttp_cancel_request(req);
				LOG_ERROR("block[%d] ftruncate(%s, %d) failed! ret=%d, error:%s",
								dlb->block_index, dlf->metafilename, meta_file_size, ret, strerror(errno));
				return ;
			}

			void * shm = mmap(NULL, meta_file_size, PROT_READ|PROT_WRITE, MAP_SHARED, dlf->metafile, 0);
			if(shm == MAP_FAILED){
				dlb->dl_err_type = dl_err_mmap_metafile;
				dlb->dl_errno = errno;
				if(errno == ENOMEM){
					LOG_ERROR("mmap failed no memory!");
				}else{
					LOG_ERROR("mmap failed! %d err:%s", errno, strerror(errno));
				}
				//3 可能有问题。
				evhttp_cancel_request(req);
				return;
			}

			dlf->metafile_size = meta_file_size;
			dlf->meta_info = (meta_info_t*)shm;
			//初始化meta_info
			sprintf(dlf->meta_info->magic, "SDL");
			dlf->meta_info->filesize = content_length;
			dlf->meta_info->block_size = dlf->block_size;
			dlf->meta_info->block_cnt = blocks(dlf->meta_info->filesize, dlf->meta_info->block_size);
			dlf->meta_info->block_downloading = 0;
			dlf->meta_info->block_download = 0;

			meta_info_set_block_downloading(dlf->meta_info, dlb->block_index);
			
			dlf_start_new_blocks_dl(dlf);
		}else{
			assert(dlf->meta_info != NULL);
		}
		
		struct evbuffer* buf = evhttp_request_get_input_buffer(req);  
	    size_t len = evbuffer_get_length(buf);
		if(len > 0){
			off_t offset = dlf->block_size*dlb->block_index+dlb->writed;
			
			int write_len = pwrite(dlf->file, evbuffer_pullup(buf, -1), len, offset);
			LOG_DEBUG("block[%d] pwrite(file:%s, len:%d, offset:%lld) = %d ", 
				dlb->block_index, dlf->filename, (int)len, (long long)offset, write_len);
			
			if(write_len != len){
				dlb->dl_err_type = dl_err_write_file;
				//3 可能有问题。
				evhttp_cancel_request(req);
				LOG_ERROR("block[%d] write dlb to [%s] failed! download len(%d) writed(%d) err:%s", 
							dlb->block_index, dlf->filename, (int)len, write_len, strerror(errno));
				return;
			}
			if(write_len > 0){
				dlb->writed += write_len;
			}
			dlb->dl_err_type = dl_ok;
		}
		//printf("################# rsp_code:%d len:%d\n", rsp_code, len);
	}else{
		LOG_ERROR("block[%d] download [%s] failed! http_code=%d", dlb->block_index, dlf->uri, rsp_code);
		dlb->dl_err_type = dl_err_http;
		dlb->dl_errno = rsp_code;
	}
	
}


// TODO: 超时处理。
int http_download_block(download_file_t* dlf,int block_index)
{
	LOG_DEBUG("begin download {uri:%s,block_index:%d,block_size:%d,"
				"localfile:%s,metafile:%s}",
				dlf->uri, block_index, dlf->block_size,
				dlf->filename, dlf->metafilename);
	//sleep(1);
	
	if(block_index==0){
		//assert(dlf->meta_info == NULL); 如果是第二次下载，依然有meta_info信息在
	}else{
		assert(dlf->meta_info != NULL);
		meta_info_t* meta_info = dlf->meta_info;
		if(meta_info_is_block_download_ok(meta_info, block_index)){
			LOG_ERROR("file[%s==>%s] block[%d] is download ok!", 
				dlf->uri, dlf->filename, block_index);
			return -1;
		}
	}

	//先检查该块是否是已经下载完的，或者正在下载中的。
	if(dlf_block_is_downloading(dlf, block_index)){
		LOG_ERROR("file[%s==>%s] block[%d] is downloading!", 
				dlf->uri, dlf->filename, block_index);
		return -1;
	}

	event_base* base = dlf->base;
	download_block_t* dlb = (download_block_t*)my_calloc(1, sizeof(download_block_t));
	dlb->block_index = block_index;
	dlb->dlf = dlf;
	dlb->writed = 0;
	dlb->dl_err_type = dl_err_connect;
	
	evhttp_request* req =  evhttp_request_new(&finish_write, dlb);
	//LOG_INFO("req: 0x%08x, dlb:0x%08x",(int)req, (int)dlb);
	evkeyvalq* header_outs = evhttp_request_get_output_headers(req);
	
	evhttp_add_header(header_outs, "Host", evhttp_uri_get_host(dlf->http_uri));
	evhttp_add_header(header_outs, "Connection", "Keep-Alive");

	char buf[32];
	memset(buf,0,sizeof(buf));
	sprintf(buf, "bytes=%d-%d", block_index* dlf->block_size, (block_index+1)*dlf->block_size-1);
	evhttp_add_header(header_outs, "Range", buf);
	evhttp_request_set_chunked_cb(req, &chunked_write);

	evhttp_connection* conn = dlf_conn_get(dlf);
	if(conn == NULL){
		LOG_ERROR("get connection from [%s:%d] failed!",dlf->http_cfg.host,dlf->http_cfg.port);
		my_free(dlb);
		dlb = NULL;
		return -1;
	}
	//evhttp_connection_connect
	dlb->conn = conn;
	
	dlf_dlblock_add(dlf, dlb);
	if(dlf->meta_info != NULL){
		meta_info_set_block_downloading(dlf->meta_info, block_index);
	}
	
	int ret = evhttp_make_request(conn, req, EVHTTP_REQ_GET, dlf->uri);
	if(ret != 0){
		LOG_ERROR("evhttp_make_request failed! ret=%d,", ret);
	}
	return ret;
}

download_file_t* http_download(event_base *base, const char* uri, char* localfile, int threads)
{
	int ret = 0;
#define BLOCK_SIZE (64*1024)

	evhttp_uri* http_uri = evhttp_uri_parse(uri);
	if(http_uri == NULL){
		LOG_ERROR("Invalid Uri: %s", uri);
		return NULL;
	}
	
	download_file_t* dlf = (download_file_t*)my_calloc(1, sizeof(download_file_t));
	if(dlf == NULL){
		LOG_ERROR("malloc download_file_t failed!");
		evhttp_uri_free(http_uri);
		http_uri = NULL;
		return NULL;
	}
	dlf->http_uri = http_uri;
	dlf->threads_max = threads;
	dlf->base = base;
	strncpy(dlf->http_cfg.host, evhttp_uri_get_host(http_uri),32);
	dlf->http_cfg.port = evhttp_uri_get_port(http_uri);
	if(dlf->http_cfg.port == -1){
		dlf->http_cfg.port = 80;
	}
	dlf->http_cfg.timeout = 5;
	
	dlf->block_size = BLOCK_SIZE;
	dlf->uri = (char*)my_strdup(uri);
	int filename_len = strlen(localfile);
	dlf->filename = (char*)my_strdup(localfile);
	dlf->metafilename = (char*)my_calloc(filename_len+8, 1);
	sprintf(dlf->metafilename, "%s.sd", localfile);
		
	dlf->metafile = open(dlf->metafilename, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	if(dlf->metafile == -1){
		LOG_ERROR("open metafile [%s] failed! err:%s", dlf->metafilename, strerror(errno));
		download_file_destroy(dlf);
		return NULL;
	}
	int64_t meta_file_size = GetFileSize(dlf->metafile);
	LOG_INFO("metafile [%s] size:%d", dlf->metafilename, meta_file_size);

	if(meta_file_size == 0 || meta_file_size==-1){//新下载。
		dlf->file = open(dlf->filename, O_CREAT|O_TRUNC| O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
		LOG_DEBUG("open localfile [%s] file=%d", dlf->filename, dlf->file);
		if(dlf->file == -1){
			LOG_ERROR("open file [%s] failed! error:%s", dlf->filename, strerror(errno));
			download_file_destroy(dlf);
			return NULL;
		}
		
		http_download_block(dlf,  0);
	}else{
		dlf->file = open(dlf->filename, O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
		LOG_DEBUG("open localfile [%s] file=%d", dlf->filename, dlf->file);
		if(dlf->file == -1){
			LOG_ERROR("open file [%s] failed! err:%s", dlf->filename, strerror(errno));
			download_file_destroy(dlf);
			return NULL;
		}
		dlf->metafile_size = meta_file_size;
		meta_info_t * meta_info = (meta_info_t*)mmap(NULL, meta_file_size, PROT_READ|PROT_WRITE, MAP_SHARED, dlf->metafile, 0);
		if(meta_info == MAP_FAILED){
			if(errno == ENOMEM){
				LOG_ERROR("mmap failed no memory!");
			}else{
				LOG_ERROR("mmap failed! %d err:%s", errno, strerror(errno));
			}
			download_file_destroy(dlf);
			return NULL;
		}

		dlf->meta_info = meta_info;
		//校验meta_info
		if(strcmp(meta_info->magic, "SDL")!=0){
			LOG_ERROR("meta file [%s] invalid!", dlf->metafilename);
			download_file_destroy(dlf);
			return NULL;
		}
		
		meta_info->block_downloading = 0;

		int x;
		int block_download = 0;
		//4 清除上次正在下载中的标记。
		for(x=0;x<meta_info->block_cnt;x++){
			//已经下载完的。
			if(meta_info_is_block_download_ok(meta_info, x)){
				block_download++;
				continue;
			}
			if(meta_info_is_block_downloading(meta_info, x)){
				meta_info_clear_block_downloading(meta_info, x);
				continue;
			}
		}
		
		dlf->meta_info->block_download = block_download;

		if(not_download_blocks(meta_info)==0){
			LOG_INFO("not any blocks to download!");
			download_file_destroy(dlf);
			dlf = NULL;
		}else{
			dlf_start_new_blocks_dl(dlf);
		}
	}

	return dlf;
	//evhttp_request_free(req);
}

#if 1

// url: http://127.0.0.1:80/files/a.tgz
int main(int argc, char* argv[])
{
	if(argc < 2){
		printf("Usage: %s <url> [localfile] [threads]\n", argv[0]);
		exit(1);
	}
	const char* url = argv[1];
	char localfile[256];
	memset(localfile,0,sizeof(localfile));

	if(argc >= 3){
		strcpy(localfile, argv[2]);
	}else{
		char* p = strrchr(url, '/');
		if(p == NULL){
			sprintf(localfile, "index.html");
		}else{
			sprintf(localfile, ++p);
		}
	}

	printf(" download [%s => %s] ...\n", url, localfile);
	
	int threads = 4;
	if(argc >= 4){
		threads = atoi(argv[3]);
		if(threads < 1){
			threads = 1;
		}
	}

	event_base *base = event_base_new();
	download_file_t* dlf = http_download(base, url, localfile, 4);
	event_base_dispatch(base);
	
	printf("############ dispatch #############\n");
	//http_conn_free(conn);
}
#endif

