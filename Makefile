

CC=gcc
CFLAGS=-g
LDFLAGS=-levent

OBJS+=Logger.o
OBJS+=metafile.o
OBJS+=downloader.o

all: downloader metainfo

downloader: $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

metainfo: metafile.c Logger.o
	$(CC) -DMETAFILE_MAIN $(CFLAGS) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

clean:
	rm -f metainfo downloader *.o core.* *.tgz* 